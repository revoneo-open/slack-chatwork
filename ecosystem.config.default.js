module.exports = {
  apps : [{
    name: 'from-chatwork-to-slack',
    script: 'hubot -a chatwork -n slack',
    watch: ['scripts'],
    env: {        
      NODE_ENV: 'development',
      PORT: 10000,
      HUBOT_CHATWORK_TOKEN: "xxxxxxxxxxxxxxxx",
      HUBOT_CHATWORK_ROOMS: "xxxxxxxx",
      HUBOT_CHATWORK_API_RATE: "1000",
    },
    env_production: {
      NODE_ENV: 'production'
    }
  },
  {
    name: 'from-slack-to-chatwork',
    script: 'hubot -a slack -n chatwordk',
    watch: ['scripts'],
    env: {        
      NODE_ENV: 'development',
      PORT: 10001,
      HUBOT_SLACK_TOKEN: "xxxxxxxxxxxxxxx",
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
