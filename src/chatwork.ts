import { Robot, Response } from './common';

const adapterName = 'chatwork';
const specificUserName = 's-kazuki';
const specificRoom = '92509712';

export = (robot: Robot): void => {
  if (robot.adapterName !== adapterName) {
    return;
  }

  robot.hear(/.*/i, (res: Response) => {
    const text: string = res.message.text;
    const userName: string = res.message.user.name;

    // this is bot's message
    if (text.indexOf('[from slack]') === 0) {
      return;
    }
    if (userName !== specificUserName) {
      return;
    }

    // start process
    const data: string = JSON.stringify({
      text: `[from: ${userName}] ${text}`,
    });
    robot
      .http(`http://localhost:10001/from-${adapterName}`)
      .header('Content-Type', 'application/json')
      .post(data)((error: Error, _res: any, _body: any) => {
      if (error) {
        res.send(error.message);
        return;
      }
    });
  });

  robot.router.post('/from-slack', (req: any, res: any) => {
    const text: string = req.body.text;
    res.end('OK');
    robot.messageRoom(specificRoom, text);
  });
};
