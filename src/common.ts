import hubot from 'hubot';

export interface Robot extends hubot.Robot<any> {
  http: Function;
  messageRoom: Function;
  router: {
    post: Function;
  };
  adapterName: string;
}

export type Response = hubot.Response<any>;
