import { Robot, Response } from './common';

const adapterName = 'slack';
const specificRoom = 'random';

export = (robot: Robot): void => {
  if (robot.adapterName !== adapterName) {
    return;
  }

  robot.hear(/.*/i, (res: Response) => {
    const text: string = res.message.text;

    // start process
    const data: string = JSON.stringify({
      text: `[from ${adapterName}] ${text}`,
    });
    robot
      .http(`http://localhost:10000/from-${adapterName}`)
      .header('Content-Type', 'application/json')
      .post(data)((error: Error, _res: any, _body: any) => {
      if (error) {
        res.send(error.message);
        return;
      }
    });
  });

  robot.router.post('/from-chatwork', (req: any, res: any) => {
    const text: string = req.body.text;
    res.end('OK');
    robot.messageRoom(specificRoom, text);
  });
};
